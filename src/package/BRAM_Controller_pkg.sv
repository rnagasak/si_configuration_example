`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: TGC ATLAS group
// Engineer: Ren Nagasaka
//
// Create Date: 2023/3/20
// Design Name:
// Module Name: BRAM_Controller_pkg
// Project Name: TAM module
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////

`ifndef  BRAM_CONTROLLER_PKG
 `define BRAM_CONTROLLER_PKG

package BRAM_Controller_pkg;

   localparam IDLE = 4'h0, RESET = 4'h1, WRITE_PARAMETER = 4'h2, ASSERT_TRIGER = 4'h3, WAIT_CONFIG_DONE = 4'h4, CHANGE_READ_ADDRESS = 4'h5, CONFIG_DONE = 4'h6;

endpackage

`endif

