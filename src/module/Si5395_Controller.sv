`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: ICEPP
// Engineer: Ren Nagasaka
// 
// Create Date: 2023/3/30
// Design Name: 
// Module Name: Si5395_Controller
// Project Name: 
// Target Devices: PS board first type
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Si5395_Controller
  import Si5395_Controller_pkg::*;   
  #(
    parameter WIDTH = 0
    )
   (
    input 	      clk,
    input 	      clk_ila,    
    input 	      reset,
    
    input 	      config_trig,
    input [WIDTH-1:0] config_parameter,
    output reg 	      config_done,
    
    output reg 	      SCK_en,
    output reg 	      SSB = 1'b1,
    output reg 	      MOSI = 1'b0,
    input 	      MISO
    );
   
   logic [3:0] 		   SPI_state = 4'b0;      
   logic [7:0] 		   config_counter = 8'h10;
   logic [3:0] 		   wait_counter = 4'd10;   
   logic [WIDTH-1:0] 	   parameter_shift_reg;
   logic [7:0] 		   page_address;
   logic [7:0] 		   page_address_prev = 8'hff;
   logic [7:0] 		   register_address;
   logic [7:0] 		   register_data;   
   logic 			   set_page_address_done = 1'b0;
   logic [15:0] 		   send_data;
   
   logic 		   posedge_config_trig;
   logic 		   reset_done;
   logic 		   posedge_config_trig_extended;
   

     gc_posedge gc_posedge_config_trig
     (
      .clk_i(clk),
      .rst_n_i(~reset),
      .data_i(config_trig),
      .pulse_o(posedge_config_trig)
      );

   gc_extend_pulse
     #(
       .g_width(10)
       )
   gc_extend_pulse
     (
      .clk_i(clk),
      .rst_n_i(~reset),
      .pulse_i(posedge_config_trig),
      .extended_o(posedge_config_trig_extended)
      );

   
   always @ (negedge clk) begin
      
      case (SPI_state)
        IDLE : begin
	   if (reset) begin
	      SPI_state <= RESET;	      
	   end	   
           else if (posedge_config_trig_extended) begin
              config_done <= 1'b0;

              SPI_state <= SET_PAGE_ADDRESS;	      
           end
           else begin
              SSB <= 1'b1;
	      
              SPI_state <= IDLE;
           end
        end 
	RESET : begin
	   parameter_shift_reg <= 'b0;
	   config_counter <= 'b0;
	   wait_counter <= 'd10;
	   page_address <= 8'hff;
	   page_address_prev <= 8'hff;
	   register_address <= 8'hff;
	   register_data <= 8'hff;
	   set_page_address_done <= 'b0;
	   send_data <= 'b0;
	   config_done <= 'b0;	   
	   reset_done <= 'b1;

	   SPI_state <= IDLE;	   
	end
	SET_PAGE_ADDRESS : begin
	   SCK_en <= 1'b0;	   
	   if (wait_counter == 4'b0) begin
              wait_counter <= 4'd10;
              SSB <= 1'b1;
	      if (parameter_shift_reg[WIDTH-1:WIDTH-8] != page_address_prev) begin
		 if (set_page_address_done == 1'b0) begin
		    send_data <= {inst_set_address, page_address_reg};
		    SPI_state <= WRITE_PAGE_ADDRESS;
		 end
		 else begin
		    send_data <= {inst_write_data, parameter_shift_reg[WIDTH-1:WIDTH-8]};
   		    parameter_shift_reg <= {parameter_shift_reg[WIDTH-9:0], 8'h00};
		    SPI_state <= WRITE_PAGE_ADDRESS;
		    page_address_prev <= parameter_shift_reg[WIDTH-1:WIDTH-8];
		 end
	      end
	      else begin
		 SPI_state <= SET_REGISTER_ADDRESS;
		 page_address_prev <= parameter_shift_reg[WIDTH-1:WIDTH-8];
		 parameter_shift_reg <= {parameter_shift_reg[WIDTH-9:0], 8'h00};
	      end
           end
           else begin
              parameter_shift_reg <= config_parameter;
              SPI_state <= SET_PAGE_ADDRESS;
              wait_counter <= wait_counter - 4'b1;
              SSB <= 1'b1;
           end
        end 
        WRITE_PAGE_ADDRESS : begin
	   SCK_en <= 1'b1;
           if (config_counter == 8'b0) begin
	      if (set_page_address_done == 1'b0) begin
		 SPI_state <= SET_PAGE_ADDRESS;
		 config_counter <= 8'h10;
		 SSB <= 1'b1;
		 set_page_address_done <= 1'b1;
		 MOSI <= 1'b0;
	      end
	      else begin
		 SPI_state <= SET_REGISTER_ADDRESS;
		 config_counter <= 8'h10;
		 SSB <= 1'b1;
		 set_page_address_done <= 1'b1;
		 MOSI <= 1'b0;
	      end 
           end
           else begin
              SPI_state <= WRITE_PAGE_ADDRESS;
              config_counter <= config_counter - 8'b1;
	      send_data <= {send_data[14:0], 1'b0};
              MOSI <= send_data[15];
              SSB <= 1'b0;
           end
        end 
	SET_REGISTER_ADDRESS : begin
	   register_address <= parameter_shift_reg[WIDTH-1:WIDTH-8];
	   set_page_address_done <= 1'b0;
	   SCK_en <= 1'b0;
	   if (wait_counter == 4'b0) begin
              wait_counter <= 4'd10;
              SSB <= 1'b1;
	      send_data <= {inst_set_address, parameter_shift_reg[WIDTH-1:WIDTH-8]};
	      SPI_state <= WRITE_REGISTER_ADDRESS;
	      parameter_shift_reg <= {parameter_shift_reg[WIDTH-9:0], 8'h00};
           end
           else begin
              SPI_state <= SET_REGISTER_ADDRESS;
              wait_counter <= wait_counter - 4'b1;
              SSB <= 1'b1;
           end
        end 
        WRITE_REGISTER_ADDRESS : begin
	   SCK_en <= 1'b1;
           if (config_counter == 8'b0) begin
	      SPI_state <= SET_REGISTER_DATA;
	      config_counter <= 8'h10;
	      SSB <= 1'b1;
	      MOSI <= 1'b0;
           end
           else begin
              SPI_state <= WRITE_REGISTER_ADDRESS;
              config_counter <= config_counter - 8'b1;
	      send_data <= {send_data[14:0], 1'b0};
              MOSI <= send_data[15];
              SSB <= 1'b0;
           end
        end 
	SET_REGISTER_DATA : begin
	   SCK_en <= 1'b0;
	   register_data <= parameter_shift_reg[WIDTH-1:WIDTH-8];
	   if (wait_counter == 4'b0) begin
              wait_counter <= 4'd10;
              SSB <= 1'b1;
	      send_data <= {inst_write_data, parameter_shift_reg[WIDTH-1:WIDTH-8]};
	      SPI_state <= WRITE_REGISTER_DATA;
	      parameter_shift_reg <= {parameter_shift_reg[WIDTH-9:0], 8'h00};
           end
           else begin
              SPI_state <= SET_REGISTER_DATA;
              wait_counter <= wait_counter - 4'b1;
              SSB <= 1'b1;
           end
        end 
        WRITE_REGISTER_DATA : begin
	   SCK_en <= 1'b1;
           if (config_counter == 8'b0) begin
	      if (|parameter_shift_reg == 1'b0) begin
	         SPI_state <= CONFIG_DONE;
	         config_counter <= 8'h10;
	         SSB <= 1'b1;
	         MOSI <= 1'b0;
	      end	      
	      else begin
		 SPI_state <= SET_PAGE_ADDRESS;
		 config_counter <= 8'h10;
		 SSB <= 1'b1;
		 MOSI <= 1'b0;
              end
	   end 
           else begin
              SPI_state <= WRITE_REGISTER_DATA;
              config_counter <= config_counter - 8'b1;
	      send_data <= {send_data[14:0], 1'b0};
              MOSI <= send_data[15];
              SSB <= 1'b0;
	   end 
	end // case: WRITE_REGISTER_DATA
	CONFIG_DONE : begin
	   config_done <= 1'b1;

	   SPI_state <= IDLE;	   
	end	
        default : begin
           SPI_state <= IDLE;
           SSB <= 1'b1;
        end
      endcase
   end // always @ (negedge clk)

   ila_0 ila_0
     (
      .clk(clk_ila),

      .probe0(SPI_state), // 4
      .probe1(config_trig),
      .probe2(config_parameter) // 24
      );

   
endmodule
