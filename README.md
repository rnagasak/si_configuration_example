# Si5395 configuration example

## Important notice
You cannot determine whether the Si5395 configured correctly or not only by output clock.
Because the Si5395 outputs the clock made by internal(in case of the Si5395) reference clock if there is no correct input clock, you have to determine by the output clock and the LOLB port of the Si5395.
The LOLB means Loss of Lock Bar, so this must be high.

This module is made for just configuraiton of the Si5395, so LOLB is not high by just bake this firmware.
Bake your own firmware after baking this firmware.

## Quick use
1. Make .csv file of the configuraiton parameter by the Clock Builder Pro.
2. Clean up this .csv file to .coe fie format.
3. Change the FINAL_ADDRESS parameter of BRAM_Controller instance in the TAM_firmware_top_Si5395.sv. If your .coe file have 24 bit data times 100 depth, FINAL_ADDRESS must be 99. This -1 comes from that the BRAM read address starts from 0.
4. Change FPGA type and .xdc file.
5. Generate bitstream.
6. Bake this firmware. Configuraiton of the Si5395 is done by this step.
7. Bake your firmware.

## Whole design
This firmware is made for the configuration of the Si5395 on the TAM board.
If you want to make your Si series configured, you have to change parameters in .coe file at src directory.
This .coe file is made by the Clock Builder Pro.

Whole configuration process is like this,
1. Start_configuration of the BRAM_Controller.sv is asserted high by the power_on port of the clocking wizard.
2. The BRAM_Controller.sv sets config_parameter (24 bit) and, after 1 tick, asserts the config_trig high.
3. The Si5395_Controller.sv detects the positive edge of the config_trig and start to operate SCK_en, SSB and MOSI according to SPI 4- wire protocol. 
4. When the Si5395_Controller.sv finished sending config_paramter (24 bit), the config_done is asserted high.
5. Then the BRAM_Controller.sv detects positive edge of the config_done and change read address (addra port) to the next read address.
6. 2~5 instruction is repeated while whole parameter is delivered to the Si5395.
7. If whole configuraiton is done, the BRAM_Controller.sv asserts end_configuration high. 

## modules

### Si5395_Controller.sv
This module operate SPI protocol.
If the config_trig port is high, this module detects positive edge of this and start to operate SSB and MOSI port according to SPI 4-wire protocol.
Input data is config_parameter which is delivered from BRAM_Controller.sv.
In Si series cases, data is composed of page address (8 bit), register address (8 bit) and register data (8bit), totally 24 binary.
When 24 bit data is deliverd to Si5395, config_done port is asserted high.

### BRAM_Controller.sv
This module operate BRAM and delivers data to Si5395_Controller.sv.
If start_configuration port is high, this module detects positive edge of this and start whole configuraiton of Si5395.
The end_configuration port must be high when whole configuration is done.
