# I/O assignment for each bank of FPGA

# BANK 33
set_property PACKAGE_PIN Y8 [get_ports SYSCLK_200p]
set_property PACKAGE_PIN Y7 [get_ports SYSCLK_200n]
set_property IOSTANDARD LVDS [get_ports SYSCLK_200p]
set_property IOSTANDARD LVDS [get_ports SYSCLK_200n]

set_property PACKAGE_PIN AA5 [get_ports SISCLK]
set_property IOSTANDARD LVCMOS18 [get_ports SISCLK]
set_property PACKAGE_PIN AB5 [get_ports SISDIO]
set_property IOSTANDARD LVCMOS18 [get_ports SISDIO]
set_property PACKAGE_PIN AB8 [get_ports SISDO]
set_property IOSTANDARD LVCMOS18 [get_ports SISDO]
set_property PACKAGE_PIN AB7 [get_ports SICSB]
set_property IOSTANDARD LVCMOS18 [get_ports SICSB]
set_property PACKAGE_PIN AA6 [get_ports SIRST_b]
set_property IOSTANDARD LVCMOS18 [get_ports SIRST_b]
set_property PACKAGE_PIN AB6 [get_ports INSEL0]
set_property IOSTANDARD LVCMOS18 [get_ports INSEL0]
set_property PACKAGE_PIN AA10 [get_ports INSEL1]
set_property IOSTANDARD LVCMOS18 [get_ports INSEL1]
set_property PACKAGE_PIN AB10 [get_ports INTR]
set_property IOSTANDARD LVCMOS18 [get_ports INTR]
set_property PACKAGE_PIN AA9 [get_ports LOLB]
set_property IOSTANDARD LVCMOS18 [get_ports LOLB]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]


create_clock -period 5.000 [get_ports SYSCLK_200p]


set_property PACKAGE_PIN G12 [get_ports LED0]
set_property PACKAGE_PIN F9 [get_ports LED1]
set_property PACKAGE_PIN E9 [get_ports LED2]
set_property PACKAGE_PIN H9 [get_ports LED3]

set_property IOSTANDARD LVCMOS33 [get_ports LED0]
set_property IOSTANDARD LVCMOS33 [get_ports LED1]
set_property IOSTANDARD LVCMOS33 [get_ports LED2]
set_property IOSTANDARD LVCMOS33 [get_ports LED3]


set_property PACKAGE_PIN AA14    [get_ports PResetB]
set_property IOSTANDARD LVCMOS33 [get_ports PResetB]
set_property PACKAGE_PIN V19     [get_ports FPGA40_i]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA40_i]

set_property PACKAGE_PIN W9  [get_ports GTRECLK_P]
set_property PACKAGE_PIN Y9  [get_ports GTRECLK_N]
set_property IOSTANDARD LVDS [get_ports GTRECLK_P]
set_property IOSTANDARD LVDS [get_ports GTRECLK_N]

set_property PACKAGE_PIN L3 [get_ports LEMOOUTP]
set_property PACKAGE_PIN M3 [get_ports LEMOOUTN]
set_property IOSTANDARD LVDS [get_ports LEMOOUTP]
set_property IOSTANDARD LVDS [get_ports LEMOOUTN]

create_clock -period 25.000 [get_ports LEMOOUTP]

create_clock -period 25.000 [get_ports FPGA40_i]

create_clock -period 25.000 [get_ports GTRECLK_P]
