`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: ICEPP
// Engineer: Ren Nagasaka
//
// Create Date: 2023/3/30
// Design Name:
// Module Name: BRAM_Controller
// Project Name:
// Target Devices: PS board first type
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module BRAM_Controller
  import BRAM_Controller_pkg::*;   
  #(
    parameter WIDTH = 0,
    parameter FINAL_ADDRESS = 0
    )
   (
    input 		   clk,
    input 		   clk_ila,    
    input 		   reset,

    input 		   start_configuration,
    output reg 		   end_configuration,

    output reg 		   config_trig,
    output reg [WIDTH-1:0] config_parameter,
    input 		   config_done
    );

   logic [3:0] 		   BRAM_state;
   logic 		   posedge_start_configuration;
   logic 		   posedge_start_configuration_extended;

   logic [WIDTH-1:0] 	   data_out;
   
   logic [9:0] 		   rd_addr;

   logic 		   wr_enb = 1'b0;
   logic [23:0] 	   data_in;
   logic 		   reset_done;

   logic [3:0] 		   wait_counter = 4'h0;
   
   

   gc_posedge gc_posedge_start_configuration
     (
      .clk_i(clk),
      .rst_n_i(~reset),
      .data_i(start_configuration),
      .pulse_o(posedge_start_configuration)
      );

   gc_extend_pulse
     #(
       .g_width(10)
       )
   gc_extend_pulse
     (
      .clk_i(clk),
      .rst_n_i(~reset),
      .pulse_i(posedge_start_configuration),
      .extended_o(posedge_start_configuration_extended)
      );

   always_ff @ (posedge clk) begin
      case (BRAM_state)
	IDLE : begin
	   if (reset) begin
	      BRAM_state <= RESET;
	   end
	   else if (posedge_start_configuration_extended) begin
	      end_configuration <= 'b0;
	      
	      BRAM_state <= WRITE_PARAMETER;
	   end
	   else begin
	      BRAM_state <= IDLE;
	   end	   	      
	end // case: IDLE
	RESET : begin
	   config_parameter <= 'b0;
	   config_trig <= 'b0;
	   rd_addr <= 'b0;
	   reset_done <= 'b1;
	   
	   BRAM_state <= IDLE;
	end
	WRITE_PARAMETER : begin
	   if (wait_counter == 4'hf) begin
	      wait_counter <= 4'h0;	      
	      config_parameter <= data_out;
	      BRAM_state <= ASSERT_TRIGER;	   
	   end
	   else begin
	      wait_counter <= wait_counter + 1;
	      BRAM_state <= WRITE_PARAMETER;
	   end	   	   	   
	end
	ASSERT_TRIGER : begin
	   config_trig <= 1'b1;	   
	   BRAM_state <= WAIT_CONFIG_DONE;
	end	
	WAIT_CONFIG_DONE : begin
	   if (config_done) begin
	      BRAM_state <= CHANGE_READ_ADDRESS;	      
	   end
	   else begin
	      BRAM_state <= WAIT_CONFIG_DONE;	      
	   end	   
	end
	CHANGE_READ_ADDRESS : begin
	   config_trig <= 1'b0;	   
	   if (rd_addr == FINAL_ADDRESS) begin
	      BRAM_state <= CONFIG_DONE;	      
	   end
	   else begin
	      rd_addr <= rd_addr + 1;
	      BRAM_state <= WRITE_PARAMETER;
	   end
	end
	CONFIG_DONE : begin
	   end_configuration <= 1'b1;
	   rd_addr <= 'b0;
	   
	   BRAM_state <= IDLE;	   
	end		
      endcase // case (BRAM_state)
   end
   
   
   blk_mem_gen_0 blk_mem_gen_0
     (
      .clka(clk),    // input wire clka
      .wea(1'b0),      // input wire [0 : 0] wea
      .addra(rd_addr),  
      .dina('b0),    // input wire [0 : 0] dina
      .douta(data_out)
      );

   ila_1 ila_1
     (
      .clk(clk_ila),

      .probe0(BRAM_state), // 4
      .probe1(start_configuration), // 1
      .probe2(end_configuration),
      .probe3(config_trig),
      .probe4(config_done),
      .probe5(data_out), //24
      .probe6(config_parameter), // 24
      .probe7(rd_addr) // 10
      );
   
endmodule
