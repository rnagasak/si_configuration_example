`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: ICEPP
// Engineer: Ren Nagasaka
//
// Create Date: 2023/3/30
// Design Name:
// Module Name: TAM_firmware_top_Si5395
// Project Name: TAM module
// Target Devices: TAM module 1st type
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module TAM_firmware_top_Si5395
  (
   //-------common port--------------
   //system clk
   input  SYSCLK_200p , // From 200MHz Oscillator module
   input  SYSCLK_200n , // From 200MHz Oscillator module
   input  PResetB ,
  
   output LED0,
   output LED1,
   output LED2,
   output LED3,
   output GTRECLK_P,
   output GTRECLK_N,
   input  FPGA40_i,

   //-----Si5345 control----------------
   //SPI control
   output SIRST_b,
   input  SISDO,
   output SISDIO,
   output SISCLK,
   output SICSB,
   //other control
   output INSEL0,
   output INSEL1,
   input  INTR,
   input  LOLB,

   output LEMOOUTP,
   output LEMOOUTN
   );

   logic  CLK_40M;
   logic  CLK_5M;
   logic  CLK_slow;
   logic  reclk_40M;
   logic  power_on;

   logic 	start_configuration;
   logic 	end_configuration;   
   logic 	config_trig;
   logic 	config_done;
   logic [23:0] config_parameter;   
   
   logic        SCK_en;   
   logic [31:0] counter_32;
      
   assign INSEL0 = 1'b0;
   assign INSEL1 = 1'b0;
   
   //----MMCM for common use
   clk_wiz_0 clk_wiz_0
     (
      .clk_out1(CLK_40M), // out
      .clk_out2(CLK_5M), // out by Nagasaka for gtwizard
      .reset(1'b0), // in
      .locked(power_on), // out
      .clk_in1_p(SYSCLK_200p), // in
      .clk_in1_n(SYSCLK_200n)
      ); // in

   Si5395_Controller
     #(
       .WIDTH(24)
       )
   Si5395_Controller
     (
      .clk(CLK_slow),
      .clk_ila(CLK_40M),
      .reset(~power_on),
      
      .config_trig(config_trig),
      .config_parameter(config_parameter),
      .config_done(config_done),
      
      .SCK_en(SCK_en),
      .SSB(SICSB),
      .MOSI(SISDIO),
      .MISO(SISDO)
      );

   BRAM_Controller
     #(
       .WIDTH(24),
       .FINAL_ADDRESS(595)
       )
   BRAM_Controller
     (
      .clk(CLK_slow),
      .clk_ila(CLK_40M),
      .reset(~power_on),
      
      .start_configuration(start_configuration),
      .end_configuration(end_configuration),
      
      .config_trig(config_trig),
      .config_parameter(config_parameter),
      .config_done(config_done)
      );
   


   assign SISCLK = (SCK_en) ? CLK_slow : 1'b0;

   assign LED0 = SISDIO;
   assign LED1 = end_configuration;
   assign LED2 = LOLB;
   assign LED3 = reclk_40M;
   
   assign CLK_slow = counter_32[6];
   
   always @ (posedge CLK_5M) begin
      counter_32 <= counter_32 + 32'b1;
      if (power_on == 1'b1) begin
	 if (counter_32[7:0] == 8'hff) begin
	    if (PResetB == 1'b0) begin
	       start_configuration <= 1'b0;
	    end
	    else begin
	       start_configuration <= 1'b1;
	    end
	 end
      end
      else begin
	 counter_32 <= 32'h00000000;
      end // else: !if(power_on == 1'b1)
   end   

   OBUFDS OBUFDS_to_Si5395
     (
      .O(GTRECLK_P),   // 1-bit output: Diff_p output (connect directly to top-level port)
      .OB(GTRECLK_N), // 1-bit output: Diff_n output (connect directly to top-level port)
      .I(CLK_40M)//.I(adjusted_clk_40M)//.I(hm_Orbit_clk_bufg)    // 1-bit input: B
      );
   BUFG BUFG_from_Si5395
     (
      .O(reclk_40M), // 1-bit output: Clock output
      .I(FPGA_40_i)  // 1-bit input: Clock input
      );

   OBUFDS OBUFDS_to_LEMOOUT
     (
      .O(LEMOOUTP),   // 1-bit output: Diff_p output (connect directly to top-level port)
      .OB(LEMOOUTN), // 1-bit output: Diff_n output (connect directly to top-level port)
      .I(reclk_40M)//.I(adjusted_clk_40M)//.I(hm_Orbit_clk_bufg)    // 1-bit input: B
      );
   

endmodule
