`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: TGC ATLAS group
// Engineer: Ren Nagasaka
//
// Create Date: 2023/3/20
// Design Name:
// Module Name: Si5395_Controller_pkg
// Project Name: TAM module
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////

`ifndef  SI5395_CONTROLLER_PKG
 `define SI5395_CONTROLLER_PKG

package Si5395_Controller_pkg;

   localparam IDLE = 4'h0, RESET = 4'h1, SET_PAGE_ADDRESS = 4'h2, WRITE_PAGE_ADDRESS = 4'h3, SET_REGISTER_ADDRESS = 4'h4, WRITE_REGISTER_ADDRESS = 4'h5, SET_REGISTER_DATA = 4'h6, WRITE_REGISTER_DATA = 4'h7, CONFIG_DONE = 4'h8;

   localparam page_address_reg = 8'b00000001, inst_set_address = 8'b00000000, inst_write_data = 01000000, inst_read_data = 10000000;

endpackage

`endif

